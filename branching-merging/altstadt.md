# Altstadt

### Soban

Zwingerstr. 21, Heidelberg, BW 69117

https://www.restaurant-soban.de/speisekarte/

#### Pro

- good, spicy Korean food

#### Con

- not many vegetarian/vegan options
- quite small - best to make a reservation!

### Kulturbrauerei Heidelberg

Leyergasse 6, 69117 Heidelberg

https://heidelberger-kulturbrauerei.de/en/

#### Pro

- traditional German food

#### Con

- sometimes overcrouded
